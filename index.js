const express = require('express');
const app = express();

const { 
    getBook,
    createBook,
    findBook,
    deleteBook,
    updateBook 
} = require('./mongo');

const Port = process.env.Port || 3000;

app.listen(Port, () => {
    console.log(`Listen at : ${Port}`);
})

app.use(express.json());

//Routes

app.get('/', (req, res) => {
    res.send('Hola mundo');
    console.log('Rutas OK');
});

app.get('/books', async(req, res) => {
    
    const result = await getBook();
    res.send( result );

});

app.get('/books/:id', async(req, res) => {
    
    const book = await findBook(req.params.id);
    if(!book) return res.status(400).send('Result not found');
    
    res.status(200).send(book);

});

app.post('/books', async(req, res) => {

    const book = req.body;

    const bookCreated = await createBook(book);
    res.status(201).send(bookCreated);

});


app.patch('/books/:id', async(req, res) => {

    const id = req.params.id;
    const modificado = req.body;

    const book = await findBook(id);
    if(!book) return res.status(400).send('Result not found');

    const books = await updateBook( id, modificado);
    res.send(books);
    
})

app.delete('/books/:id', async(req, res) => {
    
    const id = req.params.id;

    const book = await findBook(id);
    if(!book) return res.status(400).send('Result not found');

    const books = await deleteBook(id);
    res.send(books);
})