const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/books', {useNewUrlParser:true})
    .then( () => console.log('Mongo Conectado'))
    .catch( (err) => console.log("ERROR => ", err));

const booksSchema = new mongoose.Schema({
    name: String,
    author: String,
    tags: [String],
    date: {type: Date, Default: Date.now},
    isPublished: Boolean
});

const Books = mongoose.model("books", booksSchema);

async function getBook() {
    const books = await Books.find();
    return books;
}

async function createBook(books){

    const book = new Books({
        name: books.name,
        author: books.author,
        tags: books.tags,
        isPublish: books.isPublish
    })
    
    const result = await book.save();
    return result;
}

async function findBook(id){
    const result = await Books.findById(id)
    return result;
}

async function deleteBook(id){
    const result = await Books.deleteOne({ _id: id});
    return result;
}

async function updateBook( id, data ){
    const book = await Books.findByIdAndUpdate( id, {
        name: data.name,
        author: data.author,
        tags: data.tags,
        isPublished: data.isPublished,
    });

    return book;
}

module.exports = {
    getBook,
    createBook,
    findBook,
    deleteBook,
    updateBook
};